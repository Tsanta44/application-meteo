export const useGetImage = () => {
    const getImage = (cloud: number) => {
        if(cloud >= 0 && cloud <25){
            return "/assets/img/5.png"
        }
        if(cloud > 25 && cloud <50){
            return "/assets/img/2.png"
        }
        if(cloud > 50 && cloud <= 100){
            return "/assets/img/7.png"
        }
    }

    return getImage
}
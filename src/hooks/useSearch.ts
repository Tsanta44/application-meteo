import { useRecoilState } from "recoil";
import { searchText } from "../state";

/**
 * 
 * @returns the recoil array to manipulate data of the search text
 */
export const useSearch = () => {
    return useRecoilState(searchText);
};

import { useMapData } from "./useMapData";
import { useSearch } from "./useSearch";

/**
 * Get the weather data from the API
 * @returns
 */
export const useGetDatas = () => {
    const [search] = useSearch();
    const mapData = useMapData();

    const fetchData = () => {
        return fetch(
            `${process.env.REACT_APP_API_URL}?q=${search}&appid=${process.env.REACT_APP_API_KEY}&units=metric`
        )
            .then((res) => res.json())
            .then((data) => mapData(data));
    };

    return {
        fetchData,
    };
};

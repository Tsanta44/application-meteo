export const useMapData = () => {
    const getDayName = (dateString: Date) => {
        const days = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
        ];
        const d = new Date(dateString);
        return days[d.getDay()];
    }

    const mappingData = (data: any): WeatherResponse => {
        return {
            cityname: data.name,
            degree: Math.round(data.main.feels_like),
            dayNow: getDayName(new Date()),
            description: data.weather[0].description,
            wind: data.wind.speed,
            sunrise: new Date(data.sys.sunrise * 1000),
            sunset: new Date(data.sys.sunset * 1000),
            humidity: data.main.humidity,
            visibility: data.visibility/1000,
            pressure: data.main.pressure,
            cloud: data.clouds.all
        };
    };

    return mappingData;
};

export type WeatherResponse = {
    cityname: string;
    degree: number;
    dayNow: string;
    description: string;
    wind: number;
    sunrise: Date;
    sunset: Date;
    humidity: number;
    visibility: number;
    pressure: string;
    cloud: number;
};

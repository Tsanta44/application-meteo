import { atom } from "recoil";

export const searchText = atom({
    key: "search.text",
    default: "London",
});



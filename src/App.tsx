import WeatherContainer from "./containers/WeatherContainer";

const App = () => {
    return <WeatherContainer />;
};

export default App;

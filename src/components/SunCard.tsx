import { Box, Typography } from "@mui/material";
import React from "react";
import { CardWeatherInfo } from "./CardWeatherInfo";
import ArrowCircleUpIcon from "@mui/icons-material/ArrowCircleUp";
import ArrowCircleDownIcon from "@mui/icons-material/ArrowCircleDown";
import { makeStyles } from "@mui/styles";

export const SunCard = (props: SunCardProps) => {
    const classes = useStyles();

    return (
        <CardWeatherInfo title={"Sunrise & Sunset"}>
            <Box className={classes.sun}>
                <ArrowCircleUpIcon fontSize="large" />
                <Typography>{props.sunrise.getHours()}:{props.sunrise.getMinutes()}</Typography>
            </Box>
            <Box className={classes.sun}>
                <ArrowCircleDownIcon fontSize="large" />
                <Typography>{props.sunset.getHours()}:{props.sunset.getMinutes()}</Typography>
            </Box>
        </CardWeatherInfo>
    );
};

const useStyles = makeStyles({
    sun: {
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center",
        marginBottom: 5,
        width: "80%",
    },
});

type SunCardProps = {
    sunrise: Date;
    sunset: Date;
};

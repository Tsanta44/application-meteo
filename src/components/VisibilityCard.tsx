import { Typography } from "@mui/material";
import React from "react";
import { CardWeatherInfo } from "./CardWeatherInfo";

export const VisibilityCard = (props: VisibilityCardProps) => {
    return (
        <CardWeatherInfo title={"Visibility"}>
            <Typography variant="h4" component="div">
               {props.visibility} km
            </Typography>
        </CardWeatherInfo>
    );
};

type VisibilityCardProps = {
    visibility: number
}
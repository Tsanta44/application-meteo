import { Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Box } from "@mui/system";
import React from "react";

export const WeatherInfo = (props: WeatherInfoProps) => {
    const classes = useStyles();

    return (
        <Box className={classes.root} mt={5} ml={7}>
            <Typography variant="overline">{props.description}</Typography>
            <Typography className={classes.city}>{props.city}</Typography>
        </Box>
    );
};

const useStyles = makeStyles({
    root: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "flex-start"
    },
    city:{
        position: "relative",
        top: 25
    }
});

type WeatherInfoProps = {
    description: string;
    city: string;
}
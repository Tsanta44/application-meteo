import { Typography } from "@mui/material";
import React from "react";
import { CardWeatherInfo } from "./CardWeatherInfo";

export const WindStatusCard = (props: WindStatusCardProps) => {
    return (
        <CardWeatherInfo title={"Wind Status"}>
            <Typography variant="h4" component="div">
                {props.windStatus} m/s
            </Typography>
        </CardWeatherInfo>
    );
};

type WindStatusCardProps = {
    windStatus: number;
}
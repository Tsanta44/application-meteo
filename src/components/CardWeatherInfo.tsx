import { Card, CardContent, Typography } from "@mui/material";
import React from "react";

export const CardWeatherInfo = ({ title, children }: CardWeatherInfoProps) => {
    return (
        <Card sx={{ borderRadius: 3 }}>
            <CardContent>
                <Typography
                    sx={{ fontSize: 14, marginBottom: 5 }}
                    color="text.secondary"
                    gutterBottom
                >
                    {title}
                </Typography>
                {children}
            </CardContent>
        </Card>
    );
};

type CardWeatherInfoProps = {
    title: string;
    children: React.ReactNode;
};

import { Box, InputAdornment, TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import React from "react";
import { useDebounce } from "../hooks/useDebounce";
import { useSearch } from "../hooks/useSearch";

export const SearchText = () => {
    const classes = useStyles();
    const debounce = useDebounce();
    const [, setSearch] = useSearch()

    const handleChange = (
        e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
        debounce(() => setSearch(e.target.value), 750);
    };

    return (
        <Box className={classes.root}>
            <TextField
                className={classes.text}
                placeholder="Search for places ..."
                variant="outlined"
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <HighlightOffIcon />
                        </InputAdornment>
                    ),
                }}
                onChange={handleChange}
            />
        </Box>
    );
};

const useStyles = makeStyles({
    root: {
        display: "flex",
        justifyContent: "center",
        marginTop: 25,
    },
    text: {
        "& .MuiOutlinedInput-root": {
            height: 30,
            width: 280,
            fontSize: "13px",
            borderRadius: "15px",
        },
    },
});

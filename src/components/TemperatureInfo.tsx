import { Divider, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Box } from "@mui/system";
import React from "react";
import { useGetImage } from "../hooks/useGetImage";

export const TemperatureInfo = ({degree, day, cloud}: TemperatureInfoProps) => {
    const classes = useStyles();
    const getImg = useGetImage();

    return (
        <Box ml={5} mr={5} className={classes.root}>
            <img className={classes.img} src={getImg(cloud)} alt="Saison" />
            <Typography variant="h2">
                {degree}° C
            </Typography>
            <Typography variant="body1">{day}</Typography>
            <Divider />
        </Box>
    );
};

const useStyles = makeStyles({
    root:{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "center"
    },
    img: {
        width: "80%",
        height: "80%",
        marginBottom: 15
    }
});

type TemperatureInfoProps = {
    degree: number;
    day: string;
    cloud: number;
}
import { Typography } from "@mui/material";
import React from "react";
import { CardWeatherInfo } from "./CardWeatherInfo";

export const HumidityCard = (props: HumidityCardProps) => {
    return (
        <CardWeatherInfo title={"Humidity"}>
            <Typography variant="h4" component="div">
                {props.humidity} %
            </Typography>
        </CardWeatherInfo>
    );
};

type HumidityCardProps = {
    humidity: number;
}
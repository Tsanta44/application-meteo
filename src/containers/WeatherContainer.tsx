import { makeStyles } from "@mui/styles";
import { useState, useEffect } from "react";
import { useGetDatas } from "../hooks/useGetDatas";
import { WeatherResponse } from "../hooks/useMapData";
import { useSearch } from "../hooks/useSearch";
import { MainContent } from "./MainContent";
import Sidebar from "./Sidebar";

const WeatherContainer = () => {
    const classes = useStyles();
    const [search] = useSearch();
    const { fetchData } = useGetDatas();
    const [weatherData, setWeatherData] = useState<WeatherResponse>();

    useEffect(() => {
        fetchData().then((res) => setWeatherData(res));
    }, [search]);

    return (
        <div className={classes.root}>
            <Sidebar weatherData={weatherData!} />
            <MainContent weatherData={weatherData!} />
        </div>
    );
};

const useStyles = makeStyles({
    root: {
        background: "#F6F6F8",
        marginTop: 50,
        marginBottom: 50,
        marginLeft: 150,
        marginRight: 150,
        height: "85vh",
        borderRadius: "36px",
        display: "flex",
        justifyContent: "flex-start",
    },
});

export default WeatherContainer;

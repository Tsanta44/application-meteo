import { makeStyles } from "@mui/styles";
import React from "react";
import { SearchText } from "../components/SearchText";
import { TemperatureInfo } from "../components/TemperatureInfo";
import { WeatherInfo } from "../components/WeatherInfo";
import { WeatherResponse } from "../hooks/useMapData";
import _ from "lodash";

const Sidebar = ({ weatherData }: SidebarProps) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <SearchText />
            {_.isEmpty(weatherData) && "Loading ..."}
            
            {weatherData && (
                <>
                    <TemperatureInfo
                        degree={weatherData.degree}
                        day={weatherData.dayNow}
                        cloud={weatherData.cloud}
                    />
                    <WeatherInfo
                        city={weatherData.cityname}
                        description={weatherData.description}
                    />
                </>
            )}
        </div>
    );
};

const useStyles = makeStyles({
    root: {
        background: "white",
        borderTopLeftRadius: "36px",
        borderBottomLeftRadius: "36px",
        padding: 10,
        flex: 0.4,
        height: "96.5%",
        width: "20%",
    },
});

export default Sidebar;

type SidebarProps = {
    weatherData: WeatherResponse;
};

import { Box, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import _ from "lodash";
import { HumidityCard } from "../components/HumidityCard";
import { SunCard } from "../components/SunCard";
import { VisibilityCard } from "../components/VisibilityCard";
import { WindStatusCard } from "../components/WindStatusCard";
import { WeatherResponse } from "../hooks/useMapData";

export const MainContent = ({weatherData}: MainContentProps) => {
    const classes = useStyles();
 
    return (
        <Box className={classes.root}>
            <Typography variant="h6">Today's highlight</Typography>
            {_.isEmpty(weatherData) && "Loading ..."}

            {weatherData && (
                <Box className={classes.content}>
                    <WindStatusCard windStatus={weatherData.wind} />
                    <SunCard
                        sunrise={weatherData.sunrise!}
                        sunset={weatherData.sunset!}
                    />
                    <HumidityCard humidity={weatherData.humidity} />
                    <VisibilityCard visibility={weatherData.visibility} />
                </Box>
            )}
        </Box>
    );
};

const useStyles = makeStyles({
    root: {
        flex: 1,
        padding: 35,
        // marginTop: 25,
    },
    content: {
        marginTop: 25,
        display: "grid",
        gridTemplateColumns: "1fr 1fr 1fr",
        rowGap: "25px",
        columnGap: "25px",
    },
});

type MainContentProps = {
    weatherData: WeatherResponse;
}
